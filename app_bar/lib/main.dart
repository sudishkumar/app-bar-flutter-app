import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final GlobalKey <ScaffoldMessengerState>scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  final SnackBar snackBar = const SnackBar(content: Text('Notification clicked'));

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(15.0),
    child: ScaffoldMessenger( key:scaffoldKey ,
      child:Scaffold(
        appBar: AppBar(
          title: const Text("AppBar"),
        actions: <Widget>[
          IconButton(icon: const Icon(Icons.add_alert),
              tooltip: 'Show snackBar',
              onPressed: (){
                scaffoldKey.currentState.showSnackBar(snackBar);
              }),
          IconButton(icon:const Icon(Icons.close),
            tooltip: 'Close',
            onPressed: (){
            Navigator.pop(context);
          },
          )
        ],
      ),
      ) ,
    )
    );

    // return MaterialApp(
    //   title: 'Flutter Demo',
    //
    //   theme: ThemeData(
    //     primarySwatch: Colors.blue,
    //   ),
    //   home: MyHomePage(title: 'Flutter Demo Home Page'),
    // );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
